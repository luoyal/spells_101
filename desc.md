```
{
    name: '', //插件的名字
    version: '', //插件版本号
    description: '', //插件描述
    author: '', //作者名
    main: '', //入口文件路径，require(name)将根据这个路径来引入
    files: '', //文件夹名，所有文件都会被包含进项目中(除非是那些在其他规则中被忽略的文件)
    bin: {}, //许多包有一个或多个可执行文件希望被安装到系统路径。提供一个bin字段，它是一个命令名和本地文件名的映射。在安装时，如果是全局安装，npm将会使用符号链接把这些文件链接到prefix/bin，如果是本地安装，会链接到./node_modules/.bin/。
    keywords: '', //关键词，使用数组形式，方便npm官网搜索
    scripts: {}, //命令行，通过npm run 执行
    license: '', //许可证书，一般开源是MIT
    homepage: '', //项目主页的url
    repository: '', //github仓库项目地址
}
```
